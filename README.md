**Experis Academy, Norway**

**Authors:**
* **�rjan Stor�s**

# Experis_Week2_Task17

## Task 17: A more realistic GUI � RPG part 1

The purpose of this program will be for RPG character creation.

Task text:
Write a program which allows a user to create an RPG
character � Use WinForms or WPF to create a UI
* A character can be a Wizard, Warrior, Thief
* Each character type can have subtypes (be creative)
* A character has HP, Mana/Energy, Name, armorRating
* A character can attack and move
* Once the user clicks create/submit, a summary of the character
must be displayed
* This character must be created as an object and used to display the
summary as well as write the details to a text file