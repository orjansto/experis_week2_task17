﻿using System;
using Task17.Classes;
using Task17.Classes.Fighters;
using Task17.Classes.Rogues;
using Task17.Classes.Spellcasters;

namespace Task17
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
           
            Character playerOne = new Character(new Smuggler(), "Azi", "Human");
            Character playerTwo = new Character(new Warrior(), "Markus", "Gnome");
            Character playerThree = new Character(new Priest(), "Huy", "Goblin");
            playerOne.printToConsole();
            playerOne.attack();
            playerOne.move();

            Console.WriteLine();
            playerTwo.printToConsole();
            playerTwo.attack();
            playerTwo.move();
            Console.WriteLine();
            playerThree.printToConsole();
            playerThree.attack();
            playerThree.move();
            Console.WriteLine();
            playerTwo.ToFile(@"C:\Users\ostora\ExperisTasks\Experis_Week2_Task17\WriteText.txt");
           
        }
    }
}
