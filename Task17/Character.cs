﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Task17.Classes.Spellcasters;

namespace Task17
{
   
    class Character
    {
        Class charactersClass;
        string name;
        String race;
        double hp = 100;
        double armorRating = 10;
        
        /// <summary>
        /// Class for handeling RPG-Character creation
        /// </summary>
        /// <param name="charactersClass">Class</param>
        /// <param name="name">Character name</param>
        /// <param name="race">Character race</param>
        public Character(Class charactersClass, string name, string race)
        {
            this.charactersClass = charactersClass;
            this.name = name;
            this.race = race;
            armorRating *= charactersClass.ArmorModifier;
            hp *= charactersClass.HpModifier;
        }

        /// <summary>
        /// Print the RPG-Character to Console
        /// </summary>
        public void printToConsole()
        {
            Console.WriteLine("RPG-Character Specification:");
            Console.WriteLine($"Name:  {name}");
            Console.WriteLine($"Race:  {race}");
            Console.Write("Class: ");
            if (charactersClass.ClassName.Contains("Rogue"))
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
            }
            else if (charactersClass.ClassName.Contains("Fighter"))
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            else if (charactersClass.ClassName.Contains("Spellcaster"))
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
            }
            Console.WriteLine($"{charactersClass.ClassName}");
            Console.ResetColor();
            Console.WriteLine($"HP:    {hp}");
            Console.WriteLine($"AR:    {armorRating}");
        }

        /// <summary>
        /// Returns a RPG-Character to String
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string character = $"RPG-Character Specification:\n" +
                $"Name:  {name}\n" +
                $"Race:  {race}\n" +
                $"Class: {charactersClass.ClassName}\n" +
                $"HP:    {hp}\n" +
                $"AR:    {armorRating}";
            return character;
        }

        /// <summary>
        /// Writing to a .txt file at specified location.
        /// </summary>
        /// <param name="filePath">of form: "C:\Users\Public\TestFolder"</param>
        public void ToFile(string filePath)
        {
            string text = ToString();
            System.IO.File.WriteAllText(@filePath, text);
        }

        /// <summary>
        /// Write an attack to Console
        /// </summary>
        public void attack()
        {
            charactersClass.Attack();
        }
        /// <summary>
        /// Write a move to Console
        /// </summary>
        public void move()
        {
            charactersClass.Move();
        }
    }
}
