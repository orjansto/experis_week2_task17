﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17.Classes.Fighters
{
    /// <summary>
    /// A RPG Fighter Class
    /// </summary>
    class Berserker : Fighter
    {
        public Berserker()
        {
            Stamina *= 2;
            className += " Berserker";
            hpModifier *= 1.2;
            armorModifier *= 0.7;
        }

        public override void Attack()
        {
            Console.WriteLine("RAAWRW!!"); ;
        }
    }
}
