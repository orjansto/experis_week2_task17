﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17.Classes.Spellcasters
{
    /// <summary>
    /// A RPG Spellcaster class
    /// </summary>
    class Priest : Spellcaster
    {
        public Priest()
        {
            Mana *= 1.1;
            className += " Priest";
            hpModifier *= 0.9;
            armorModifier *= 0.9;
        }
    }
}
